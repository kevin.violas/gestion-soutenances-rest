-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Ven 05 Janvier 2018 à 13:57
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dbsilex`
--

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

CREATE TABLE `enseignant` (
  `id` int(11) NOT NULL,
  `nom` varchar(40) NOT NULL,
  `prenom` varchar(40) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `detail` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `enseignant`
--

INSERT INTO `enseignant` (`id`, `nom`, `prenom`, `photo`, `detail`) VALUES
(1, 'Arsouze', 'Julien', '..', 'bref'),
(4, 'us', 'anonym', 'https://pbs.twimg.com/profile_images/824716853989744640/8Fcd0bji.jpg', 'fzdg');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` int(11) NOT NULL,
  `nom` varchar(40) DEFAULT NULL,
  `prenom` varchar(40) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `detail` varchar(200) DEFAULT NULL,
  `groupe` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etudiant`
--

INSERT INTO `etudiant` (`id`, `nom`, `prenom`, `photo`, `detail`, `groupe`) VALUES
(2, 'rider', 'kevin', 'https://nerdist.com/wp-content/uploads/2016/09/ghost-rider-featured.jpg', 'ggg', 1),
(15, 'llf', 'bref', 'lzdf', 'dfz', 1),
(16, 'aze', 'aze', 'http://dreamicus.com/data/ghost/ghost-03.jpg', 'aze', 2),
(17, 'violas', 'kevin', 'none', 'etudiant', 1);

-- --------------------------------------------------------

--
-- Structure de la table `etudiant_groupesoutenance`
--

CREATE TABLE `etudiant_groupesoutenance` (
  `id` int(11) NOT NULL,
  `etudiant_id` int(11) NOT NULL,
  `groupesoutenance_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etudiant_groupesoutenance`
--

INSERT INTO `etudiant_groupesoutenance` (`id`, `etudiant_id`, `groupesoutenance_id`) VALUES
(73, 2, 1),
(74, 15, 1),
(75, 16, 3),
(76, 16, 4),
(77, 17, 5);

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `id` int(11) NOT NULL,
  `nom` varchar(40) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `enseignant` int(11) NOT NULL,
  `groupe` int(11) NOT NULL,
  `dureeSoutenances` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `evenement`
--

INSERT INTO `evenement` (`id`, `nom`, `date_debut`, `date_fin`, `enseignant`, `groupe`, `dureeSoutenances`) VALUES
(1, 'soutenances', '2018-01-15', '2018-01-17', 1, 1, 10),
(10, 'event test', '2018-01-12', '2018-01-16', 1, 2, 10);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id` int(11) NOT NULL,
  `nom` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `groupe`
--

INSERT INTO `groupe` (`id`, `nom`) VALUES
(1, 'LP1'),
(2, 'LP2'),
(5, 'LP3');

-- --------------------------------------------------------

--
-- Structure de la table `groupesoutenance`
--

CREATE TABLE `groupesoutenance` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `evenement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `groupesoutenance`
--

INSERT INTO `groupesoutenance` (`id`, `nom`, `evenement`) VALUES
(1, 'GroupeSoutenance1', 1),
(3, 'GroupeSoutenance2', 1),
(4, 'GroupeSoutenanceLP2', 10),
(5, 'GroupeSoutenance3', 1);

-- --------------------------------------------------------

--
-- Structure de la table `slot`
--

CREATE TABLE `slot` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `heure_debut` time NOT NULL,
  `heure_fin` time NOT NULL,
  `salle` varchar(10) NOT NULL,
  `evenement` int(11) NOT NULL,
  `enseignant` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `slot`
--

INSERT INTO `slot` (`id`, `date`, `heure_debut`, `heure_fin`, `salle`, `evenement`, `enseignant`) VALUES
(1, '2018-01-15', '08:20:00', '08:30:00', 'i03', 1, 'Arsouze'),
(2, '2018-01-15', '08:30:00', '08:40:00', 'i21', 10, 'Arsouze'),
(3, '2018-01-15', '08:00:00', '08:10:00', 'i03', 1, 'Arsouze'),
(4, '2018-01-15', '08:10:00', '08:20:00', 'i03', 1, 'Arsouze');

-- --------------------------------------------------------

--
-- Structure de la table `soutenance`
--

CREATE TABLE `soutenance` (
  `id` int(11) NOT NULL,
  `slot` int(11) NOT NULL,
  `groupeSoutenance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `soutenance`
--

INSERT INTO `soutenance` (`id`, `slot`, `groupeSoutenance`) VALUES
(5, 1, 1),
(8, 2, 4),
(14, 3, 3),
(15, 4, 5);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `enseignant`
--
ALTER TABLE `enseignant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groupe` (`groupe`);

--
-- Index pour la table `etudiant_groupesoutenance`
--
ALTER TABLE `etudiant_groupesoutenance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `etudiant_id` (`etudiant_id`),
  ADD KEY `groupesoutenance_id` (`groupesoutenance_id`);

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `enseignant` (`enseignant`),
  ADD KEY `groupe` (`groupe`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `groupesoutenance`
--
ALTER TABLE `groupesoutenance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evenement` (`evenement`);

--
-- Index pour la table `slot`
--
ALTER TABLE `slot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evenement` (`evenement`);

--
-- Index pour la table `soutenance`
--
ALTER TABLE `soutenance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slot` (`slot`),
  ADD KEY `groupeSoutenance` (`groupeSoutenance`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `enseignant`
--
ALTER TABLE `enseignant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `etudiant_groupesoutenance`
--
ALTER TABLE `etudiant_groupesoutenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT pour la table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `groupesoutenance`
--
ALTER TABLE `groupesoutenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `slot`
--
ALTER TABLE `slot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `soutenance`
--
ALTER TABLE `soutenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `etudiant_ibfk_1` FOREIGN KEY (`groupe`) REFERENCES `groupe` (`id`);

--
-- Contraintes pour la table `etudiant_groupesoutenance`
--
ALTER TABLE `etudiant_groupesoutenance`
  ADD CONSTRAINT `etudiant_groupesoutenance_ibfk_1` FOREIGN KEY (`groupesoutenance_id`) REFERENCES `groupesoutenance` (`id`),
  ADD CONSTRAINT `etudiant_groupesoutenance_ibfk_2` FOREIGN KEY (`etudiant_id`) REFERENCES `etudiant` (`id`);

--
-- Contraintes pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD CONSTRAINT `evenement_ibfk_1` FOREIGN KEY (`enseignant`) REFERENCES `enseignant` (`id`),
  ADD CONSTRAINT `evenement_ibfk_2` FOREIGN KEY (`groupe`) REFERENCES `groupe` (`id`);

--
-- Contraintes pour la table `groupesoutenance`
--
ALTER TABLE `groupesoutenance`
  ADD CONSTRAINT `groupesoutenance_ibfk_1` FOREIGN KEY (`evenement`) REFERENCES `evenement` (`id`);

--
-- Contraintes pour la table `slot`
--
ALTER TABLE `slot`
  ADD CONSTRAINT `slot_ibfk_1` FOREIGN KEY (`evenement`) REFERENCES `evenement` (`id`);

--
-- Contraintes pour la table `soutenance`
--
ALTER TABLE `soutenance`
  ADD CONSTRAINT `soutenance_ibfk_1` FOREIGN KEY (`slot`) REFERENCES `slot` (`id`),
  ADD CONSTRAINT `soutenance_ibfk_2` FOREIGN KEY (`groupeSoutenance`) REFERENCES `groupesoutenance` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
