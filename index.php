<div class="container">
<?php
    require_once __DIR__ ."/vendor/autoload.php";
    require_once "connect.php";
    require_once "modele.php";

    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Silex\Provider\TwigServiceProvider;

    Request::enableHttpMethodParameterOverride();

    $app = new Silex\Application();
    $app["debug"]= true;
    $app->register(new TwigServiceProvider(), array('twig.path' => __DIR__ . '/', ));

    $app->get("/", function() use ($app) {
        return $app['twig']->render("templates/home.html.twig");
    })->bind("home");

    # routes étudiant

    $app->get("/etudiants", function() use ($app) {
        $etudiants = new Etudiant;
        $groupe = new Groupe;
        $groupes = $groupe->get_groupes();
        $grp = array();
        foreach($groupes as $g) {
            $grp[$g['id']] = $g['nom'];
        }
        return $app['twig']->render("templates/etudiants.html.twig", array(
            "etudiants" => $etudiants->get_etudiants(),
            "groupes" => $grp
        ));
    })->bind("etudiants");

    $app->get("/etudiants/add", function() use ($app) {
        $groupe = new Groupe;
        $groupes = $groupe->get_groupes();
        return $app['twig']->render("templates/ajout_etudiant.html.twig", array(
            "groupes" => $groupes
        ));
    })->bind("add_etu");

    $app->post("/etudiants/ajout", function(Request $request) use ($app) {
        $prenom = $request->get("prenom");
        $nom = $request->get("nom");
        $photo = $request->get("photo");
        $detail = $request->get("detail");
        $groupe = $request->get("groupe");
        $etu = new Etudiant;
        $etu->add_etudiant($prenom, $nom, $photo, $detail, $groupe);
        return $app['twig']->render("templates/ajout_etudiant_success.html.twig");
    })->bind("ajout_etu");

    $app->get("/etudiants/delete/{id}", function($id) use ($app) {
        $etu = new Etudiant;
        $etudiant = $etu->get_etudiant_by_id($id);
        return $app['twig']->render("templates/confirmer_suppression_etudiant.html.twig", array(
            "etudiant" => $etudiant
        ));
    })->bind("suppr_etu");

    $app->delete("/etudiants/suppr/{id}", function($id) use ($app) {
        $etu = new Etudiant;
        $etudiant = $etu->get_etudiant_by_id($id);
        $etu->suppr_etudiant($id);
        return $app['twig']->render("templates/suppression_success_etudiant.html.twig", array(
            "etudiant" => $etudiant
        ));
    })->bind("conf_suppr_etu");

    $app->get("/etudiants/modif/{id}", function($id) use ($app) {
        $etu = new Etudiant;
        $etudiant = $etu->get_etudiant_by_id($id);
        $groupe = new Groupe;
        $groupes = $groupe->get_groupes();
        return $app['twig']->render("templates/modif_etudiant.html.twig", array(
            "groupes" => $groupes,
            "etudiant" => $etudiant
        ));
    })->bind("modif_etu");

    $app->put("/etudiants/update", function(Request $request) use ($app) {
        $id = $request->get("id");
        $prenom = $request->get("prenom");
        $nom = $request->get("nom");
        $photo = $request->get("photo");
        $detail = $request->get("detail");
        $groupe = $request->get("groupe");
        $etudiant = new Etudiant;
        $etudiant->modif_etudiant($id, $prenom, $nom, $photo, $detail, $groupe);
        return $app['twig']->render("templates/modif_success_etudiant.html.twig");
    })->bind("update_etu");

    # routes groupe

    $app->get("/groupes", function() use ($app) {
        $groupe = new Groupe;
        $groupes = $groupe->get_groupes();
        return $app['twig']->render("templates/groupes.html.twig", array(
            "groupes" => $groupes
        ));
    })->bind("groupes");

    $app->get("/groupes/add", function() use ($app) {
        return $app['twig']->render("templates/ajout_groupe.html.twig");
    })->bind("add_groupe");

    $app->post("/groupes/ajout", function(Request $request) use ($app) {
        $nom = $request->get("nom");
        $groupe = new Groupe;
        $groupe->add_groupe($nom);
        return $app['twig']->render("templates/ajout_groupe_success.html.twig");
    })->bind("ajout_groupe");

    $app->get("/groupes/delete/{id}", function($id) use ($app) {
        $grp = new Groupe;
        $groupe = $grp->get_groupe_by_id($id);
        return $app['twig']->render("templates/confirmer_suppression_groupe.html.twig", array(
            "groupe" => $groupe
        ));
    })->bind("suppr_groupe");

    $app->delete("/groupes/suppr/{id}", function($id) use ($app) {
        $grp = new Groupe;
        $groupe = $grp->get_groupe_by_id($id);
        $grp->suppr_groupe($id);
        return $app['twig']->render("templates/suppression_success_groupe.html.twig", array(
            "groupe" => $groupe
        ));
    })->bind("conf_suppr_groupe");

    $app->get("/groupes/modif/{id}", function($id) use ($app) {
        $groupe = new Groupe;
        $grp = $groupe->get_groupe_by_id($id);
        return $app['twig']->render("templates/modif_groupe.html.twig", array(
            "groupe" => $grp,
        ));
    })->bind("modif_groupe");

    $app->put("/groupes/update", function(Request $request) use ($app) {
        $id = $request->get("id");
        $nom = $request->get("nom");
        $groupe = new Groupe;
        $groupe->modif_groupe($id, $nom);
        return $app['twig']->render("templates/modif_success_groupe.html.twig");
    })->bind("update_groupe");

    // routes enseignant

    $app->get("/enseignants", function() use ($app) {
        $enseignant = new Enseignant;
        $enseignants = $enseignant->get_enseignants();
        return $app['twig']->render("templates/enseignants.html.twig", array(
            "enseignants" => $enseignants
        ));
    })->bind("enseignants");

    $app->get("/enseignants/add", function() use ($app) {
        return $app['twig']->render("templates/ajout_enseignant.html.twig");
    })->bind("add_enseignant");

    $app->post("/enseignants/ajout", function(Request $request) use ($app) {
        $prenom = $request->get("prenom");
        $nom = $request->get("nom");
        $photo = $request->get("photo");
        $detail = $request->get("detail");
        $enseignant = new Enseignant;
        $enseignant->add_enseignant($prenom, $nom, $photo, $detail);
        return $app['twig']->render("templates/ajout_enseignant_success.html.twig");
    })->bind("ajout_enseignant");

    $app->get("/enseignants/delete/{id}", function($id) use ($app) {
        $enseignant = new Enseignant;
        $ens = $enseignant->get_enseignant_by_id($id);
        return $app['twig']->render("templates/confirmer_suppression_enseignant.html.twig", array(
            "enseignant" => $ens
        ));
    })->bind("suppr_enseignant");

    $app->delete("/enseignants/suppr/{id}", function($id) use ($app) {
        $ens = new Enseignant;
        $enseignant = $ens->get_enseignant_by_id($id);
        $ens->suppr_enseignant($id);
        return $app['twig']->render("templates/suppression_success_enseignant.html.twig", array(
            "enseignant" => $enseignant
        ));
    })->bind("conf_suppr_enseignant");

    $app->get("/enseignants/modif/{id}", function($id) use ($app) {
        $ens = new Enseignant;
        $enseignant = $ens->get_enseignant_by_id($id);
        return $app['twig']->render("templates/modif_enseignant.html.twig", array(
            "enseignant" => $enseignant,
        ));
    })->bind("modif_enseignant");

    $app->put("/enseignants/update", function(Request $request) use ($app) {
        $id = $request->get("id");
        $nom = $request->get("nom");
        $prenom = $request->get("prenom");
        $photo = $request->get("photo");
        $detail = $request->get("detail");
        $enseignant = new Enseignant;
        $enseignant->modif_enseignant($id, $prenom, $nom, $photo, $detail);
        return $app['twig']->render("templates/modif_success_enseignant.html.twig");
    })->bind("update_enseignant");

    # Routes d'événement

    $app->get("/evenements", function() use ($app) {
        $evenement = new Evenement;
        $evenements = $evenement->get_evenements();
        $enseignant = new Enseignant;
        $enseignants = $enseignant->get_enseignants();
        $ens = array();
        foreach($enseignants as $e) {
            $ens[$e['id']] = $e['nom']; /*pour afficher le nom du proffesseur responsable
            sinon cela afficherait juste son id*/
        }
        $groupe = new Groupe;
        $groupes = $groupe->get_groupes();
        $grp = array();
        foreach($groupes as $g) {
            $grp[$g['id']] = $g['nom']; //pour afficher le nom du groupe
        }
        return $app['twig']->render("templates/evenements.html.twig", array(
            "evenements" => $evenements,
            "enseignant" => $ens,
            "groupe" => $grp
        ));
    })->bind("evenements");

    $app->get("/evenements/add", function() use ($app) {
        $enseignant = new Enseignant;
        $ens = $enseignant->get_enseignants();
        $groupe = new Groupe;
        $groupes = $groupe->get_groupes();
        return $app['twig']->render("templates/ajout_evenement.html.twig", array(
            "enseignants" => $ens,
            "groupes" => $groupes
        ));
    })->bind("add_evenement");

    $app->post("/evenements/ajout", function(Request $request) use ($app) {
        $nom = $request->get("nom");
        $debut = $request->get("date_debut");
        $fin = $request->get("date_fin");
        $enseignant = $request->get("enseignant");
        $groupe = $request->get("groupe");
        $duree = $request->get("dureeSoutenances");
        $event = new Evenement;
        $event->add_evenement($nom, $debut, $fin, $enseignant, $groupe, $duree);
        return $app['twig']->render("templates/ajout_evenement_success.html.twig");
    })->bind("ajout_evenement");

    $app->get("/evenements/detail/{id}", function($id) use ($app) {
        $ev = new Evenement;
        $event = $ev->get_evenement_by_id($id);
        $enseignant = new Enseignant;
        $enseignants = $enseignant->get_enseignants();
        $ens = array();
        foreach($enseignants as $e) {
            $ens[$e['id']] = $e['nom']; /*pour afficher le nom du proffesseur responsable
            sinon cela afficherait juste son id*/
        }
        $groupe = new Groupe;
        $groupes = $groupe->get_groupes();
        $grp = array();
        foreach($groupes as $g) {
            $grp[$g['id']] = $g['nom']; //pour afficher le nom du groupe
        }
        $GS = new GroupeSoutenance;
        $gs = $GS->get_groupe_soutenance_by_event($id);

        //récupération de la liste des slots
        $slot = new Slot;
        $slots = $slot->get_slots_by_event($id);

        //récupération de la liste des soutenances (pour avoir le groupe)
        $soutenance = new Soutenance;
        $soutenances = $soutenance->get_soutenances();
        $sout = array();
        foreach($soutenances as $s) {
          $sout[$s['slot']] = $s['groupeSoutenance'];
        }

        //récupération de la liste des groupes de soutenance
        $g = new GroupeSoutenance;
        $grpsoutenances = $g->get_groupes_soutenances();
        $GS = array();
        foreach($grpsoutenances as $g) {
          $GS[$g['id']] = $g['nom'];
        }

        return $app['twig']->render("templates/detail_evenement.html.twig", array(
            "evenement" => $event,
            "enseignant" => $ens,
            "groupe" => $grp,
            "groupesoutenance" => $gs,
            "slots" => $slots,
            "soutenance" => $sout,
            "groupes_soutenances" => $GS
        ));
    })->bind("detail_evenement");

    $app->get("/evenements/delete/{id}", function($id) use ($app) {
        $ev = new Evenement;
        $event = $ev->get_evenement_by_id($id);
        return $app['twig']->render("templates/confirmer_suppression_evenement.html.twig", array(
            "evenement" => $event
        ));
    })->bind("suppr_evenement");

    $app->delete("/evenements/suppr/{id}", function($id) use ($app) {
        $ev = new Evenement;
        $event = $ev->get_evenement_by_id($id);
        $ev->suppr_evenement($id);
        return $app['twig']->render("templates/suppression_success_evenement.html.twig", array(
            "evenement" => $event
        ));
    })->bind("conf_suppr_evenement");

    $app->get("/evenements/modif/{id}", function($id) use ($app) {
        $ev = new Evenement;
        $event = $ev->get_evenement_by_id($id);
        $enseignant = new Enseignant;
        $enseignants = $enseignant->get_enseignants();
        $groupe = new Groupe;
        $groupes = $groupe->get_groupes();
        return $app['twig']->render("templates/modif_evenement.html.twig", array(
            "evenement" => $event,
            "enseignants" => $enseignants,
            "groupes" => $groupes
        ));
    })->bind("modif_evenement");

    $app->put("/evenements/update", function(Request $request) use ($app) {
        $id = $request->get("id");
        $nom = $request->get("nom");
        $debut = $request->get("date_debut");
        $fin = $request->get("date_fin");
        $enseignant = $request->get("enseignant");
        $groupe = $request->get("groupe");
        $duree = $request->get("dureeSoutenances");
        $event = new Evenement;
        $event->modif_evenement($id, $nom, $debut, $fin, $enseignant, $groupe, $duree);
        return $app['twig']->render("templates/modif_success_evenement.html.twig");
    })->bind("update_evenement");

    # gestion des Groupes de Soutenance

    $app->get("/groupeSoutenance", function() use ($app) {
      $groupeSoutenance = new GroupeSoutenance;
      $groupesSoutenance = $groupeSoutenance->get_groupes_soutenances(); // nom et evenement
      $etu_groupeSoutenance = new Etudiant_GroupeSoutenance;
      $etudiants = $etu_groupeSoutenance->get_etudiant_groupe_soutenance(); // liste des étudiants
      $etu = new Etudiant;
      $liste = $etu->get_etudiants();
      $etus = array();
      foreach($liste as $l) {
          $etus[$l['id']] = $l['prenom']." ".$l['nom'];
      }
      $event = new Evenement;
      $events = $event->get_evenements();
      $evenements = array();
      foreach($events as $e) {
          $evenements[$e['id']] = $e['nom'];
      }
      return $app['twig']->render("templates/groupesoutenance.html.twig", array(
        "groupes" => $groupesSoutenance,
        "etudiants" => $etudiants,
        "liste" => $etus,
        "max" => count($etudiants)-1,
        "events" => $evenements
      ));
    })->bind("groupesoutenance");

    $app->get("/groupesoutenance/add", function() use ($app) {
      $etudiant = new Etudiant;
      $etudiants = $etudiant->get_etudiants();
      $evenement = new Evenement;
      $evenements = $evenement->get_evenements();
      $groupe = new Groupe;
      $groupes = $groupe->get_groupes();
      $grp = array();
      foreach($groupes as $g) {
          $grp[$g['id']] = $g['nom'];
      }
      return $app['twig']->render("templates/ajout_groupesoutenance.html.twig", array(
        "etudiants" => $etudiants,
        "events" => $evenements,
        "groupe" => $grp
      ));
    })->bind("add_groupesoutenance");

    $app->post("/groupesoutenance/ajout", function(Request $request) use ($app) {
      $nom = $request->get("nom");
      $etudiants = $request->get("etudiants");
      $evenement = $request->get("event");
      // ajout du groupe de soutenance
      $GS = new GroupeSoutenance;
      $test = $GS->get_groupes_soutenances();
      $ids = array();
      foreach ($test as $t) {
        $ids[] = intval($t['id']);
      }
      if(!empty($ids)) {
        $id = max($ids)+1;// nouvel id de groupe soutenance
      }
      else {
        $id = 1;
      }

      $verif = $GS->verif_group_exists($etudiants, $evenement);

      if(empty($verif)) { // cas où le ou les étudiants ne sont pas dans une soutenance pour cet événement
          $GS->add_groupe_soutenance($id, $nom,$evenement);
          // ajout de la liste d'étudiants dans Etudiant_GroupeSoutenance
          $E_GS = new Etudiant_GroupeSoutenance;
          foreach ($etudiants as $etu) { // chaque étudiant
            $E_GS->add_etudiant_groupe_soutenance($etu, $id);
          }
          return $app['twig']->render("templates/ajout_groupesoutenance_success.html.twig");
      }
      else {
        $etu = new Etudiant;
        $liste = $etu->get_etudiants();
        $etus = array();
        foreach($liste as $l) {
            $etus[$l['id']] = $l['prenom']." ".$l['nom'];
        }
        $event = new Evenement;
        $events = $event->get_evenements();
        $evenements = array();
        foreach($events as $e) {
            $evenements[$e['id']] = $e['nom'];
        }
        return $app['twig']->render("templates/erreur_ajout_groupesoutenance.html.twig", array(
            "etus" => $verif,
            "etudiants" => $etus,
            "event" => $evenement,
            "events" => $evenements
        ));
      }

    })->bind("ajout_groupesoutenance");

    $app->get("/groupesoutenance/delete/{id}", function($id) use ($app) {
        $GS = new GroupeSoutenance;
        $groupe = $GS->get_groupe_soutenance_id($id);
        $event = new Evenement;
        $events = $event->get_evenements();
        $evenements = array();
        foreach($events as $e) {
            $evenements[$e['id']] = $e['nom'];
        }
        return $app['twig']->render("templates/confirmer_suppression_groupesoutenance.html.twig", array(
            "groupe" => $groupe,
            "events" => $evenements
        ));
    })->bind("suppr_groupesoutenance");

    $app->delete("/groupesoutenance/suppr/{id}", function($id) use ($app) {
        $E_GS = new Etudiant_GroupeSoutenance;
        $E_GS->suppr_etudiant_groupe_soutenance($id);
        $GS = new GroupeSoutenance;
        $groupe = $GS->get_groupe_soutenance_id($id); // pour afficher le groupe qui a été supprimé
        $GS->suppr_groupe_soutenance($id);
        return $app['twig']->render("templates/suppression_success_groupesoutenance.html.twig", array(
            "groupe" => $groupe
        ));
    })->bind("conf_suppr_groupesoutenance");

    $app->get("/groupesoutenance/modif/{id}", function($id) use ($app) {
        $GS = new GroupeSoutenance;
        $groupe = $GS->get_groupe_soutenance_id($id);
        $event = new Evenement;
        $events = $event->get_evenements();
        $etudiant = new Etudiant;
        $etudiants = $etudiant->get_etudiants();
        $E_GS = new Etudiant_GroupeSoutenance;
        $etus = $E_GS->get_etudiants($id); // etudiants déjà sélectionnés
        return $app['twig']->render("templates/modif_groupesoutenance.html.twig", array(
            "groupe" => $groupe,
            "events" => $events,
            "etudiants" => $etudiants,
            "etus" => $etus
        ));
    })->bind("modif_groupesoutenance");

    $app->put("/groupesoutenance/update", function(Request $request) use ($app) {
        $id = $request->get("id");
        $nom = $request->get("nom");
        $etudiants = $request->get("etudiants");
        $evenement = $request->get("event");
        $GS = new GroupeSoutenance;
        $E_GS = new Etudiant_GroupeSoutenance;

        $save_etu = $E_GS->get_etudiants($id); // ancien groupe

        $test = $GS->get_groupes_soutenances();

        $E_GS->suppr_etudiant_groupe_soutenance($id);

        $verif = $GS->verif_group_exists($etudiants, $evenement);

        if(empty($verif)) { // cas où le ou les étudiants ne sont pas dans une soutenance pour cet événement
            foreach ($etudiants as $etu) { // chaque étudiant
                $E_GS->add_etudiant_groupe_soutenance($etu, $id);
            }
            $GS->modif_groupe_soutenance($id, $nom, $evenement);
            return $app['twig']->render("templates/modif_success_groupesoutenance.html.twig");
        }
        else {
            foreach ($save_etu as $etu) { // remet le groupe précédent en cas d'échec
                $E_GS->add_etudiant_groupe_soutenance($etu['etudiant_id'], $id);
            }
            $etu = new Etudiant;
            $liste = $etu->get_etudiants();
            $etus = array();
            foreach($liste as $l) {
                $etus[$l['id']] = $l['prenom']." ".$l['nom'];
            }
            $event = new Evenement;
            $events = $event->get_evenements();
            $evenements = array();
            foreach($events as $e) {
                $evenements[$e['id']] = $e['nom'];
            }
            return $app['twig']->render("templates/erreur_modif_groupesoutenance.html.twig", array(
                "id" => $id,
                "etus" => $verif,
                "etudiants" => $etus,
                "event" => $evenement,
                "events" => $evenements
            ));
      }
    })->bind("update_groupesoutenance");

    $app->post("/slot/ajout", function(Request $request) use ($app) {
      $gs = $request->get("groupe");
      $date = $request->get("date");
      $salle = $request->get("salle");
      $heure_debut  = $request->get("heure").':00';
      $heure_fin = $request->get("heure_fin").':00';
      $event = $request->get("id_event");
      $ens = $request->get("enseignant");
      $ev = new Evenement;
      $evenement = $ev->get_evenement_by_id($event); // récupération de l'événement associé au slot

      $slot = new Slot;
      $slots = $slot->get_slots();
      $soutenance = new Soutenance;
      $dispo = true;
      $ids = array();
      $liste_slots = array();

      $erreur_salle = false;
      $erreur_groupe = false;
      $erreur_prof = false;
      $msg_salle = "";
      $msg_groupe = "";
      $msg_prof = "";
      $no_error = true;

      foreach ($slots as $s) {
        if($s['date'] == $date && $s['salle'] == $salle && (($heure_debut >= $s['heure_debut']) && ($heure_debut < $s['heure_fin'])) || $s['date'] == $date && $s['salle'] == $salle & (($heure_fin > $s['heure_debut']) && ($heure_fin <= $s['heure_fin']))) {
          $dispo = false;
          $erreur_salle = true;
          $no_error = false;
          $msg_salle = "La salle ".$salle." n'est pas disponible le ".$date." de ".$heure_debut." à ".$heure_fin;
          // salle indisponible sur ce créneau
        }

        if($s['date'] == $date && $ens == $s['enseignant'] && (($heure_debut >= $s['heure_debut']) && ($heure_debut < $s['heure_fin'])) || $s['date'] == $date && $ens == $s['enseignant'] && (($heure_fin > $s['heure_debut']) && ($heure_fin <= $s['heure_fin']))) {
          // l'enseignant a déjà une soutenance à cette date et dans ce créneau
          $dispo = false;
          $erreur_prof = true;
          $no_error = false;
          $msg_prof = "L'enseignant ".$ens." n'est pas disponible le ".$date." de ".$heure_debut." à ".$heure_fin;
        }
        $ids[] = intval($s['id']);
        $liste_slots[$s['id']] = $s;
      }

      $g = new GroupeSoutenance;
      $groupes_soutenance = $g->get_groupes_soutenances();
      $grps = array();
      foreach($groupes_soutenance as $e) {
          $grps[$e['id']] = $e['nom'];
      }

      $soutenances = $soutenance->get_soutenances();
      foreach ($soutenances as $so) {
        // $liste_slots[$so['slot']]; accès au slot associé à la soutenance
        if($liste_slots[$so['slot']]['evenement'] == $event && $gs == $so['groupeSoutenance']) {
          // un groupe ne peut avoir qu'une seule soutenance pour le même event
          $dispo = false;
          $erreur_groupe = true;
          $no_error = false;
          $eve = new Evenement;
          $eves = $eve->get_evenements();
          $liste = array();
          foreach($eves as $e) {
              $liste[$e['id']] = $e['nom'];
          }
          $msg_groupe = "Le groupe ".$grps[$so['groupeSoutenance']]." a déjà une soutenance pour l'événement ".$liste[$event];
        }
      }

      if($dispo) {
        if(!empty($ids)) {
          $id = max($ids)+1; // sert pour trouver l'id du nouveau slot (sert pour l'ajout de soutenance)
        }
        else {
          $id = 1;
        }
        $slot->add_slot($id, $date, $heure_debut, $heure_fin, $salle, $event, $ens);
        $soutenance = new Soutenance;
        $soutenance->add_soutenance($id, $gs);
      }

      if($no_error) {
        return $app['twig']->render("templates/ajout_success_slot.html.twig", array(
          "id" => $event
        ));
      }
      else {
        return $app['twig']->render("templates/erreur_ajout_slot.html.twig", array(
          "erreur_salle" => $erreur_salle,
          "erreur_prof" => $erreur_prof,
          "erreur_groupe" => $erreur_groupe,
          "msg_salle" => $msg_salle,
          "msg_prof" => $msg_prof,
          "msg_groupe" => $msg_groupe,
          "id" => $event
        ));
      }
    })->bind("ajout_slot");

    $app->get("/slot/delete/{id}", function($id) use ($app) {
      $enseignant = new Enseignant;
      $enseignants = $enseignant->get_enseignants();
      $ens = array();
      foreach($enseignants as $e) {
          $ens[$e['id']] = $e['nom']; /*pour afficher le nom du proffesseur responsable
          sinon cela afficherait juste son id*/
      }
      $groupe = new Groupe;
      $groupes = $groupe->get_groupes();
      $grp = array();
      foreach($groupes as $g) {
          $grp[$g['id']] = $g['nom']; //pour afficher le nom du groupe
      }
      $GS = new GroupeSoutenance;
      $gs = $GS->get_groupe_soutenance_by_event($id);

      //récupération de la liste des slots
      $slot = new Slot;
      $slots = $slot->get_slots_by_id($id);

      return $app['twig']->render("templates/confirmer_suppression_slot.html.twig", array(
        "enseignant" => $ens,
        "groupe" => $grp,
        "groupesoutenance" => $gs,
        "slots" => $slots
      ));
    })->bind("suppr_slot");

    $app->delete("/slot/suppr/{id}", function($id) use ($app) {
      $sout = new Soutenance;
      $sout_act = $sout->get_soutenances_by_slot($id);
      $slot_id = $sout_act[0]['slot'];
      $sout->delete_soutenance($id);
      $slot = new Slot;
      $s = $slot->get_slots_by_id($slot_id);
      $id_event = $s[0]['evenement'];
      $slot->delete_slot($slot_id);
      return $app['twig']->render("templates/suppression_success_slot.html.twig", array(
        'id' => $id_event
      ));
    })->bind("conf_suppr_slot");

    $app->run();
?>
</div>
