La navigation se fait principalement grâce à la barre de navigation horizontale en haut du site.

Il y a plusieurs onglets de cette barre de navigation :
  - L'onglet étudiant qui permet d'ajouter, de modifier ainsi que de supprimer des étudiants qui pourront être dans un groupe de soutenance et avoir une soutenance pour chaque événement
  - L'onglet groupes qui permet d'ajouter, de modifier et de supprimer des groupes (par exemple LP1), un élève appartient forcément à un groupe
  - L'onglet enseignants qui permet d'ajouter, de modifier et de supprimer des professeurs qui pourront être responsables d'événements
  - L'onglet événement permet de gérer les événements (ajout, suppression, modification). Un événement contient le professeur responsable et le groupe concerné (LP1) par exemple. Lorsque l'on clique sur le bouton consulter, nous pouvons voir les slots déjà attribués pour cet événement ainsi qu'un bouton qui affiche un formulaire permettant d'ajouter un nouveau slot. Pour ce faire, le php va vérifier trois choses et afficher les erreurs s'il y en a
    1. On vérifie que la salle choisie sois disponible à la date et pendant le créneau choisi dans le formulaire
    2. On vérifie que le professeur responsable soit disponible et que donc il n'ait pas deux soutenances le même jour à la même heure
    3. On vérifie que le groupe de soutenance n'a pas déjà de slot pour cet événement
  Dans le cas où toutes ces conditions sont remplies l'ajout sera effectué, sinon les erreurs qui ont eu lieu s'afficheront (donc 1, 2 ou 3 avec des précisions pour expliquer pourquoi l'ajout a échoué)
  - L'onglet groupe de soutenance permet de gérer les groupes de soutenances qui participeront à une soutenance. Ces groupes peuvent être d'une, de deux personnes ou plus selon le type d'événement mais un élève ne peut être que dans un seul groupe de soutenance pour un événement (sinon cela affiche une erreur pour que l'utilisateur puisse comprendre)
