<?php

class BaseDonnees {
	private static $connexion;

	function __construct(){
    $dsn="mysql:dbname=".BASE.";host=".SERVER;
    try{
      self::$connexion=new PDO($dsn,USER,PASSWD,
        array(PDO::ATTR_PERSISTENT =>false)
        );
    }
    catch(PDOException $e){
      printf("Échec de la connexion : %s\n", $e->getMessage());
    }
	}

	function get_connexion() {
		return self::$connexion;
	}
}

class Etudiant {
	private $base;
	private $connexion;

	function __construct() {
		$this->base = new BaseDonnees;
		$this->connexion = $this->base->get_connexion();
	}

	function get_etudiants() {
		$sql = "SELECT * from etudiant";
	  	$data = $this->connexion->query($sql);
	  	return $data;
	}

	function get_etudiant_by_id($id) {
		$sql = "SELECT * FROM etudiant WHERE id=".$id;
		$data = $this->connexion->query($sql);
		return $data;
	}

	function add_etudiant($prenom, $nom, $photo, $detail, $groupe) {
		$sql = 'INSERT INTO etudiant(prenom,nom,photo,detail,groupe) VALUES("'.$prenom.'","'.
			$nom.'","'.$photo.'","'.$detail.'","'.$groupe.'")';
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function suppr_etudiant($id) {
		$sql = 'DELETE FROM etudiant WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function modif_etudiant($id, $prenom, $nom, $photo, $detail, $groupe) {
		$sql = 'UPDATE etudiant SET prenom="'.$prenom.'", nom="'.$nom.'", photo="'.$photo.'", detail="'.$detail.
		'", groupe='.$groupe.' WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}
}

class Groupe {
	private $base;
	private $connexion;

	function __construct() {
		$this->base = new BaseDonnees;
		$this->connexion = $this->base->get_connexion();
	}

	function get_groupes() {
		$sql = "SELECT * FROM groupe";
		$data = $this->connexion->query($sql);
		return $data;
	}

	function get_groupe_by_id($id) {
		$sql = "SELECT * FROM groupe WHERE id=".$id;
		$data = $this->connexion->query($sql);
		return $data;
	}

	function add_groupe($nom) {
		$sql = 'INSERT INTO groupe(nom) VALUES("'.$nom.'")';
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function suppr_groupe($id) {
		$sql = 'DELETE FROM groupe WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function modif_groupe($id, $nom) {
		$sql = 'UPDATE groupe SET nom="'.$nom.'" WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}
}

class Enseignant {
	private $base;
	private $connexion;

	function __construct() {
		$this->base = new BaseDonnees;
		$this->connexion = $this->base->get_connexion();
	}

	function get_enseignants() {
		$sql = "SELECT * from enseignant";
	  	$data = $this->connexion->query($sql);
	  	return $data;
	}

	function get_enseignant_by_id($id) {
		$sql = "SELECT * from enseignant WHERE id=".$id;
	  	$data = $this->connexion->query($sql);
	  	return $data;
	}

	function add_enseignant($prenom, $nom, $photo, $detail) {
		$sql = 'INSERT INTO enseignant(prenom,nom,photo,detail) VALUES("'.$prenom.'","'.
			$nom.'","'.$photo.'","'.$detail.'")';
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function suppr_enseignant($id) {
		$sql = 'DELETE FROM enseignant WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function modif_enseignant($id, $prenom, $nom, $photo, $detail) {
		$sql = 'UPDATE enseignant SET prenom="'.$prenom.'", nom="'.$nom.'", photo="'.$photo.'", detail="'.$detail.
		'" WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}
}

class Evenement {
	private $base;
	private $connexion;

	function __construct() {
		$this->base = new BaseDonnees;
		$this->connexion = $this->base->get_connexion();
	}

	function get_evenements() {
		$sql = "SELECT * from evenement";
	  	$data = $this->connexion->query($sql);
	  	return $data;
	}

	function get_evenement_by_id($id) {
		$sql = "SELECT * from evenement WHERE id=".$id;
	  	$data = $this->connexion->query($sql);
	  	return $data;
	}

	function add_evenement($nom, $date_debut, $date_fin, $enseignant, $groupe, $duree) {
		$sql = 'INSERT INTO evenement(nom,date_debut,date_fin,enseignant,groupe,dureeSoutenances) VALUES("'.$nom.'","'.
			$date_debut.'","'.$date_fin.'","'.$enseignant.'","'.$groupe.'","'.$duree.'")';
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function suppr_evenement($id) {
		$sql = 'DELETE FROM evenement WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function modif_evenement($id, $nom, $deb, $fin, $ens, $groupe, $duree) {
		$sql = 'UPDATE evenement SET nom="'.$nom.'", date_debut="'.$deb.'", date_fin="'.$fin.'", enseignant="'.$ens.'", groupe="'.$groupe.'", dureeSoutenances="'.$duree.
		'" WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}
}

class GroupeSoutenance {
	private $base;
	private $connexion;

	function __construct() {
		$this->base = new BaseDonnees;
		$this->connexion = $this->base->get_connexion();
	}

	function get_groupes_soutenances() {
		$sql = "SELECT * from groupesoutenance";
  	$data = $this->connexion->query($sql);
  	return $data;
	}

	function get_groupe_soutenance_id($id) {
		$sql = "SELECT * from groupesoutenance WhERE id='".$id."'";
  	$data = $this->connexion->query($sql);
  	return $data;
	}

	function get_groupe_soutenance_by_event($id) {
		$sql = "SELECT * from groupesoutenance WhERE evenement='".$id."'";
  	$data = $this->connexion->query($sql);
  	return $data;
	}

	function verif_group_exists($new_etus, $event) {
		$groupes = $this->get_groupes_soutenances(); // groupes existants

		$etudiant = new Etudiant_GroupeSoutenance;
		$etudiants = $etudiant->get_etudiant_groupe_soutenance();

		$erreurs = array();

		foreach($groupes as $g) {
			if($g['evenement'] == $event) {
				foreach($etudiants as $etu) {
					if(in_array($etu['etudiant_id'], $new_etus) && $g['id'] == $etu['groupesoutenance_id']) {
						$erreurs[] = $etu['etudiant_id'];
					}
				}
			}
		}

		return $erreurs;
	}

	function add_groupe_soutenance($id, $nom, $event) {
		$sql = "INSERT INTO groupesoutenance(id, nom,evenement) VALUES('".$id."','".$nom."','".$event."')";
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function suppr_groupe_soutenance($id) {
		$sql = "DELETE FROM groupesoutenance where id='".$id."'";
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function modif_groupe_soutenance($id, $nom, $event) {
		$sql = 'UPDATE groupesoutenance SET nom="'.$nom.'", evenement="'.$event.'" WHERE id='.$id;
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}
}

class Etudiant_GroupeSoutenance {
	private $base;
	private $connexion;

	function __construct() {
		$this->base = new BaseDonnees;
		$this->connexion = $this->base->get_connexion();
	}

	function get_etudiant_groupe_soutenance() {
		$sql = "SELECT * from etudiant_groupesoutenance";
		$data = $this->connexion->query($sql);
		return $data->fetchAll();
	}

	function get_etudiants($id) {
		$sql = "SELECT etudiant_id from etudiant_groupesoutenance where groupesoutenance_id='".$id."'";;
		$data = $this->connexion->query($sql);
		return $data->fetchAll();
	}

	function add_etudiant_groupe_soutenance($etu, $groupe) {
		$sql = "INSERT INTO etudiant_groupesoutenance(etudiant_id, groupesoutenance_id) VALUES('".$etu."','".$groupe."')";
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function suppr_etudiant_groupe_soutenance($id) {
		$sql = "DELETE FROM etudiant_groupesoutenance where groupesoutenance_id='".$id."'";
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}
}

class Slot {
	private $base;
	private $connexion;

	function __construct() {
		$this->base = new BaseDonnees;
		$this->connexion = $this->base->get_connexion();
	}

	function get_slots() {
		$sql = "SELECT * from slot";
		$data = $this->connexion->query($sql);
		return $data->fetchAll();
	}

	function get_slots_by_event($id) {
		$sql = "SELECT * from slot where evenement=".$id." order by date asc, heure_debut";
		$data = $this->connexion->query($sql);
		return $data->fetchAll();
	}

	function get_slots_by_id($id) {
		$sql = "SELECT * from slot where id=".$id;
		$data = $this->connexion->query($sql);
		return $data->fetchAll();
	}

	function add_slot($id, $date, $deb, $fin, $salle, $event, $ens) {
		$sql = "INSERT INTO slot(id, date, heure_debut, heure_fin, salle, evenement, enseignant)
		VALUES('".$id."','".$date."','".$deb."','".$fin."','".$salle."','".$event."','".$ens."')";
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function delete_slot($id) {
		$sql = "DELETE FROM slot where id='".$id."'";
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}
}

class Soutenance {
	private $base;
	private $connexion;

	function __construct() {
		$this->base = new BaseDonnees;
		$this->connexion = $this->base->get_connexion();
	}

	function get_soutenances() {
		$sql = "SELECT * from soutenance";
		$data = $this->connexion->query($sql);
		return $data->fetchAll();
	}

	function get_soutenances_by_slot($id) {
		$sql = "SELECT * from soutenance where slot=".$id;
		$data = $this->connexion->query($sql);
		return $data->fetchAll();
	}

	function add_soutenance($id, $gs) {
		$sql = "INSERT INTO soutenance(slot, groupeSoutenance) VALUES('".$id."','".$gs."')";
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}

	function delete_soutenance($id) { // supprime la soutenance associé au slot
		$sql = "DELETE FROM soutenance where slot='".$id."'";
		$stmt = $this->connexion->prepare($sql);
		return $stmt->execute();
	}
}
